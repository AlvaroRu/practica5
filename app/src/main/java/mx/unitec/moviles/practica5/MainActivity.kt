package mx.unitec.moviles.practica5

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import mx.unitec.moviles.practica5.helper.ESHelper
import java.io.File

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var almacenamiento = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sprAlmacenamiento.onItemSelectedListener = this

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.almacenamiento_array,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        sprAlmacenamiento.adapter = adapter
    }

    fun saveFile(v: View){
        val file = etxtFile.text.toString()
        val data = etxtData.text.toString()
        if(almacenamiento == "INTERNO" ){
            this.openFileOutput(file, Context.MODE_PRIVATE).use {
                it.write(data.toByteArray())
            }
        }else if (almacenamiento == "EXTERNO"){
            if (ESHelper.isWritable()){
                Toast.makeText(this, "No almacenamiento externo", Toast.LENGTH_LONG).show()
                return
            }
            val archivo = File(this.getExternalFilesDir(""), file)
            archivo.writeBytes(data.toByteArray())
            Toast.makeText(this, getExternalFilesDir("").toString(), Toast.LENGTH_LONG).show()

        }
    }

    fun viewFile(v: View){
        val file = etxtFile.text.toString()
        etxtData.text.clear()
        if(almacenamiento == "INTERNO") {
            this.openFileInput(file).use {
               val text = it.bufferedReader().use{
                   it.readText()
               }
                etxtData.setText(text)
            }
        }else if (almacenamiento == "EXTERNO"){
            if (!ESHelper.isReadable()){
                Toast.makeText(this, "No almacenamiento externo", Toast.LENGTH_LONG).show()
                return
            }
            val archivo = File(this.getExternalFilesDir(""), file)
            val text = archivo.bufferedReader().use{
                it.readText()
            }
        }
    }
    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        almacenamiento = p0?.getItemAtPosition(p2).toString()
    }
}