package mx.unitec.moviles.practica5.Service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService

class MyJobIntnetService: JobIntentService() {
    val TAG = "MyFirstJob"
    override fun onHandleWork(intent: Intent) {
        val max = intent.getIntExtra( "max", -1)
        for (i in 0 until max) {
            Log.d(TAG, "working: Número $i" )
            //Dummy
            try {
                Thread.sleep(1000)
            }catch (e: InterruptedException){
                e.printStackTrace()
            }
            //Fin Dummy
        }
    }

    companion object {
        private const val JOB_ID = 2

        fun enquework(context: Context, intent: Intent){
            enquework(context, MyJobIntnetService::class.java, JOB_ID, Intent)
        }
    }

}